window.addEventListener('DOMContentLoaded', () => {
    const { ipcRenderer } = require('electron');

    document.getElementById('encrypt-button').addEventListener('click', () => {
        const filePath = document.getElementById('file-path').value;
        const password = document.getElementById('password').value;
        ipcRenderer.send('encrypt-file', filePath, password);
    });

    document.getElementById('decrypt-button').addEventListener('click', () => {
        const filePath = document.getElementById('file-path').value;
        const password = document.getElementById('password').value;
        ipcRenderer.send('decrypt-file', filePath, password);
    });

    document.getElementById('browse-button').addEventListener('click', () => {
        ipcRenderer.send('browse-file');
    });

    ipcRenderer.on('file-selected', (event, filePath) => {
        document.getElementById('file-path').value = filePath;
    });

    ipcRenderer.on('file-operation-result', (event, message) => {
        document.getElementById('result').textContent = message;
    });
});
