const fs = require('fs');
const crypto = require('crypto');

const algorithm = 'aes-256-cbc';

function encrypt(text, password) {
    const key = crypto.scryptSync(password, 'salt', 32);
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return iv.toString('hex') + ':' + encrypted;
}

function decrypt(text, password) {
    const parts = text.split(':');
    const iv = Buffer.from(parts.shift(), 'hex');
    const encryptedText = parts.join(':');
    const key = crypto.scryptSync(password, 'salt', 32);
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

function overwriteWithRandomWords(filePath, callback) {
    const words = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"];
    const randomContent = Array.from({ length: 1000 }, () => words[Math.floor(Math.random() * words.length)]).join(' ');

    fs.writeFile(filePath, randomContent, 'utf8', (err) => {
        if (err) {
            console.error('Error overwriting file:', err);
        } else {
            console.log('File overwritten with random words.');
            randomizeFileContent(filePath, callback);
        }
    });
}

function randomizeFileContent(filePath, callback) {
    const interval = setInterval(() => {
        const words = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"];
        const randomContent = Array.from({ length: 1000 }, () => words[Math.floor(Math.random() * words.length)]).join(' ');

        fs.writeFile(filePath, randomContent, 'utf8', (err) => {
            if (err) {
                console.error('Error randomizing file content:', err);
            } else {
                console.log('File content randomized.');
            }
        });
    }, 1000);

    setTimeout(() => {
        clearInterval(interval);
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error('Error deleting file:', err);
            } else {
                console.log('File successfully deleted.');
                if (callback) callback();
            }
        });
    }, 5000); // Randomize for 5 seconds before deleting
}

function main() {
    const args = process.argv.slice(2);
    const filePath = args[0];
    const password = args[1];
    const action = args[2];

    if (!filePath || !password || !action) {
        console.log('Usage: node cryptoText.js <file> <password> <encrypt|decrypt>');
        return;
    }

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading file:', err);
            return;
        }

        let result;
        if (action === 'encrypt') {
            result = encrypt(data, password);
            fs.writeFile(filePath, result, 'utf8', (err) => {
                if (err) {
                    console.error('Error writing file:', err);
                } else {
                    console.log('File successfully encrypted.');
                }
            });
        } else if (action === 'decrypt') {
            try {
                result = decrypt(data, password);
                fs.writeFile(filePath, result, 'utf8', (err) => {
                    if (err) {
                        console.error('Error writing file:', err);
                    } else {
                        console.log('File successfully decrypted.');
                    }
                });
            } catch (error) {
                console.error('Decryption failed. Overwriting file with random words...');
                overwriteWithRandomWords(filePath);
            }
        } else {
            console.log('Invalid action. Use "encrypt" or "decrypt".');
            return;
        }
    });
}

main();
