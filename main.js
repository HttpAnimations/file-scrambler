const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

const algorithm = 'aes-256-cbc';

function encrypt(text, password) {
    const key = crypto.scryptSync(password, 'salt', 32);
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return iv.toString('hex') + ':' + encrypted;
}

function decrypt(text, password) {
    const parts = text.split(':');
    const iv = Buffer.from(parts.shift(), 'hex');
    const encryptedText = parts.join(':');
    const key = crypto.scryptSync(password, 'salt', 32);
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

function overwriteWithRandomWords(filePath, callback) {
    const words = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"];
    const randomContent = Array.from({ length: 1000 }, () => words[Math.floor(Math.random() * words.length)]).join(' ');

    fs.writeFile(filePath, randomContent, 'utf8', (err) => {
        if (err) {
            console.error('Error overwriting file:', err);
        } else {
            console.log('File overwritten with random words.');
            randomizeFileContent(filePath, callback);
        }
    });
}

function randomizeFileContent(filePath, callback) {
    const interval = setInterval(() => {
        const words = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit"];
        const randomContent = Array.from({ length: 1000 }, () => words[Math.floor(Math.random() * words.length)]).join(' ');

        fs.writeFile(filePath, randomContent, 'utf8', (err) => {
            if (err) {
                console.error('Error randomizing file content:', err);
            } else {
                console.log('File content randomized.');
            }
        });
    }, 1000);

    setTimeout(() => {
        clearInterval(interval);
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error('Error deleting file:', err);
            } else {
                console.log('File successfully deleted.');
                if (callback) callback();
            }
        });
    }, 5000); // Randomize for 5 seconds before deleting
}

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false
        }
    });

    win.loadFile('index.html');
}

ipcMain.on('encrypt-file', (event, filePath, password) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            event.reply('file-operation-result', 'Error reading file.');
            return;
        }

        const encryptedData = encrypt(data, password);
        fs.writeFile(filePath, encryptedData, 'utf8', (err) => {
            if (err) {
                event.reply('file-operation-result', 'Error writing file.');
            } else {
                event.reply('file-operation-result', 'File successfully encrypted.');
            }
        });
    });
});

ipcMain.on('decrypt-file', (event, filePath, password) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            event.reply('file-operation-result', 'Error reading file.');
            return;
        }

        try {
            const decryptedData = decrypt(data, password);
            fs.writeFile(filePath, decryptedData, 'utf8', (err) => {
                if (err) {
                    event.reply('file-operation-result', 'Error writing file.');
                } else {
                    event.reply('file-operation-result', 'File successfully decrypted.');
                }
            });
        } catch (error) {
            console.error('Decryption failed. Overwriting file with random words...');
            overwriteWithRandomWords(filePath, () => {
                event.reply('file-operation-result', 'Decryption failed. File overwritten and deleted.');
            });
        }
    });
});

ipcMain.on('browse-file', async (event) => {
    const result = await dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [
            { name: 'Text Files', extensions: ['txt'] },
            { name: 'All Files', extensions: ['*'] }
        ]
    });

    if (result.canceled) {
        event.reply('file-selected', '');
    } else {
        event.reply('file-selected', result.filePaths[0]);
    }
});

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
